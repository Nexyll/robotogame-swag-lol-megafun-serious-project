package logic.core;

import data.model.*;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import logic.engine.Engine;

import java.util.ArrayList;
import java.util.Random;

public class Game {

    private Environment environment;
    private Engine engine;
    private ArrayList<Robot> robots = new ArrayList<>();
    private ArrayList<Ressource> ressources = new ArrayList<>();
    private IntegerProperty turnNumber = new SimpleIntegerProperty(0);

    /**
     *
     * @param _x hauteur de la map
     * @param _y largeur de la map
     * @param _robots nombre de robots à ajouter
     * @param _ressources nombre de ressources à ajouter
     */
    public Game(int _x, int _y, int _robots, int _ressources) {
        initialize(_x, _y, _robots, _ressources);
        engine = new Engine(environment, robots);
    }

    public void nextTurn(){
        engine.next();
        turnNumber.add(1);
    }

    public void initialize(int _x, int _y, int _robots, int _ressources){
        Random rand = new Random();
        environment= new Environment(_x, _y);
        for (int i = 0; i < _robots; i++) {
            boolean robotEmptySquare = false;
            Square s = null;
            while (robotEmptySquare == false)
            {
                s = environment.getSquares().get(rand.nextInt((_x*_y) - 1));
                if (s.getRobot() == null) robotEmptySquare =  true;
            }
            Robot r = new Robot();
            robots.add(r);
            s.setRobot(r);
        }
        for (int j = 0; j < _ressources; j++) {
            Square s = environment.getSquares().get(rand.nextInt((_x*_y) -1));
            int randTypeRessouce = rand.nextInt(100);
            Ressource r = new Ressource(randTypeRessouce < 50 ? TypeRessource.OR : TypeRessource.FER);
            ressources.add(r);
            s.setRessource(r);
        }
    }

    public Environment getEnvironment() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment = environment;
    }

    public ArrayList<Robot> getRobots() {
        return robots;
    }

    public void setRobots(ArrayList<Robot> robots) {
        this.robots = robots;
    }

    public ArrayList<Ressource> getRessources() {
        return ressources;
    }

    public void setRessources(ArrayList<Ressource> ressources) {
        this.ressources = ressources;
    }

    public Engine getEngine() {
        return engine;
    }
}
