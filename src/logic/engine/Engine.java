package logic.engine;

import data.command.ICommand;
import data.model.Environment;
import data.model.PartialEnvironment;
import data.model.Robot;
import data.model.Square;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Engine implements IEngine {
    private Environment environment;
    private ArrayList<Robot> robots;
    private static final int robotDistanceView = 1;

    /**
     *
     * @param environment Le modèle avec lequel le moteur va travailler
     * @param robots Liste des robots présents sur l'environement
     */
    public Engine(Environment environment, ArrayList<Robot> robots) {
        this.environment = environment;
        this.robots = robots;
    }

    /**
     * Demande au moteur de lancer et superviser un tour.
     */

    @Override
    public void next(){
        /* VERSION THREAD
        HashMap<Robot, ICommand> commandsToExecute = new HashMap<>();
        robots.forEach(x -> commandsToExecute.put(x, x.chooseCommand(generatePartialFor(x))));
        for (Map.Entry<Robot, ICommand> keyPairValue:commandsToExecute.entrySet()) {
            try {
                keyPairValue.getValue().execute();
            } catch (Exception e) {
                try {
                    keyPairValue.getKey().chooseCommand(generatePartialFor(keyPairValue.getKey())).execute();
                } catch (Exception e1) {
                    ; // c'est très oui.
                }
            }
        }
        */
        for (Robot r : robots) {
            try {
                r.chooseCommand(generatePartialFor(r)).execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public PartialEnvironment generatePartialFor(Robot r)
    {
        // On récupère toutes les cases à une distance inférieure ou égale à 1 par rapport au robot
        /*ArrayList<Square> accesibleSquares = environment.getSquares().stream()
                .filter(x -> x.getCoordinate().distance(environment.findRobotSquare(r).getCoordinate()) <= robotDistanceView)
                .collect(Collectors.toCollection(ArrayList::new));*/

        ArrayList<Square> accesibleSquares = new ArrayList<>();
        for (Square square : environment.getSquares()) {
            int distance = ((int) square.getCoordinate().distance(environment.findRobotSquare(r).getCoordinate()));
            if (distance <= robotDistanceView) accesibleSquares.add(square);
        }

        PartialEnvironment res = new PartialEnvironment(1+2*robotDistanceView, 1+2*robotDistanceView);
        accesibleSquares.forEach(x -> res.getSquares().add(x));
        return res;
    }

}
