package gui;

import data.model.Square;
import data.model.TypeRessource;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.paint.Color;

public class SquareViewModel {
    private ObjectProperty<Square> square = new SimpleObjectProperty<>();

    private ObjectProperty<Color> robotColor = new SimpleObjectProperty<>();
    private ObjectProperty<Color> ressourceColor = new SimpleObjectProperty<>();

    public SquareViewModel(){
        robotColor.setValue(Color.BLACK);
    }

    public void initData(Square s)
    {
        square.setValue(s);

        robotColor.setValue(s.getRobot()==null ? Color.BLACK : Color.RED);
        ressourceColor.setValue(s.getRessource()==null ? Color.BLACK : s.getRessource().getType() == TypeRessource.OR ? Color.YELLOW : Color.GRAY);

        square.get().robotProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue != null) robotColor.setValue(Color.RED);
            else robotColor.setValue(Color.BLACK);
        });
        square.get().ressourceProperty().addListener((observable, oldValue, newValue) -> {
            ressourceColor.setValue(newValue==null ? Color.BLACK : newValue.getType() == TypeRessource.OR ? Color.YELLOW : Color.GRAY);
        });
    }

    public Color getRobotColor() {
        return robotColor.get();
    }

    public ObjectProperty<Color> robotColorProperty() {
        return robotColor;
    }

    public void setRobotColor(Color robotColor) {
        this.robotColor.set(robotColor);
    }

    public Color getRessourceColor() {
        return ressourceColor.get();
    }

    public ObjectProperty<Color> ressourceColorProperty() {
        return ressourceColor;
    }

    public void setRessourceColor(Color ressourceColor) {
        this.ressourceColor.set(ressourceColor);
    }
}
