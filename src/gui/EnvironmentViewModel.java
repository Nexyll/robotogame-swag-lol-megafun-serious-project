package gui;

import data.model.Environment;
import data.model.Square;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.GridPane;

import java.io.IOException;

public class EnvironmentViewModel {
    private ObjectProperty<Environment> environment = new SimpleObjectProperty<>();

    @FXML
    private GridPane environmentGrid;

    public EnvironmentViewModel(){
    }

    public void initData(Environment e){
        environment.setValue(e);
        for (Square square : environment.get().getSquares()) {
            try {
                FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("SquareView.fxml"));
                environmentGrid.add(fxmlLoader.load(), square.getCoordinate().x, square.getCoordinate().y);
                SquareViewModel squareViewModel = fxmlLoader.getController();
                squareViewModel.initData(square);

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        }
    }

    public Environment getEnvironment() {
        return environment.get();
    }

    public ObjectProperty<Environment> environmentProperty() {
        return environment;
    }

    public void setEnvironment(Environment environment) {
        this.environment.set(environment);
    }

    public void next(ActionEvent actionEvent) {
    }
}
