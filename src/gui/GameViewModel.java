package gui;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import logic.core.Game;

import java.io.IOException;

public class GameViewModel {
    @FXML
    private AnchorPane environmentView;
    private Game game;

    public void initData(int mapSize, int nbRobots, int nbRessources){
        game = new Game(mapSize, mapSize, nbRobots, nbRessources);
        environmentView.getChildren().clear();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("EnvironmentView.fxml"));
            environmentView.getChildren().add(fxmlLoader.load());
            EnvironmentViewModel environmentViewModel = fxmlLoader.getController();
            environmentViewModel.initData(game.getEnvironment());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public void next(ActionEvent actionEvent) {
        game.nextTurn();
    }

    public void keyAction(KeyEvent keyEvent) {
        if (keyEvent.getCode() == KeyCode.F) game.nextTurn();
    }
}
