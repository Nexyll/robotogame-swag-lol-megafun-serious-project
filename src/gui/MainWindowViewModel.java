package gui;



import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Spinner;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;

public class MainWindowViewModel {

    private StringProperty console = new SimpleStringProperty("No game created");

    @FXML
    private Spinner<Integer> spinnerMapSize;
    @FXML
    private Spinner<Integer> spinnerNbRobots;
    @FXML
    private Spinner<Integer> spinnerNbRessources;

    @FXML
    private AnchorPane mainView;

    public MainWindowViewModel(){
    }


    @FXML
    public void launch(ActionEvent actionEvent) {
        mainView.getChildren().clear();
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("GameView.fxml"));
            mainView.getChildren().add(fxmlLoader.load());
            GameViewModel gameViewModel = fxmlLoader.getController();
            gameViewModel.initData(spinnerMapSize.getValueFactory().getValue(), spinnerNbRobots.getValueFactory().getValue(), spinnerNbRessources.getValueFactory().getValue());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public String getConsole() {
        return console.get();
    }

    public StringProperty consoleProperty() {
        return console;
    }

    public void setConsole(String console) {
        this.console.set(console);
    }
}
