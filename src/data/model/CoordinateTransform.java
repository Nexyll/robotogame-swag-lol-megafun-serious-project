package data.model;

import java.awt.*;

public final class CoordinateTransform {
    public static int transform(int x, int y, int sizeX)
    {
        return x + sizeX*y;
    }

    public static Point transform(int i, int width)
    {
        return new Point(i % width, i / width);
    }
}
