package data.model;

import java.util.ArrayList;

public class Environment {
    protected int sizeX;
    protected int sizeY;

    private ArrayList<Square> squares = new ArrayList<>();

    public Environment(int _x, int _y) {
        sizeX = _x;
        sizeY = _y;
        for (int i = 0; i < _y; i++) {
            for (int j = 0; j < _x; j++) {
                squares.add(new Square(j, i));
            }
        }

    }

    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < sizeY; i++) {
            for (int j = 0; j < sizeX; j++) {
                res.append(squares.get(i* sizeX + j).toString());
            }
            res.append("\n");
        }
        return res.toString();
    }

    public ArrayList<Square> getSquares() {
        return squares;
    }

    public Square findRobotSquare(Robot r)
    {
        for (Square s : squares)
        {
            if (s.getRobot() == r) return s;
        }
        return null;
    }

    public int getSizeX() {
        return sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }
}
