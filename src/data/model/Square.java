package data.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

import java.awt.*;

public class Square {


    private ObjectProperty<Robot> robot = new SimpleObjectProperty<>();
    private ObjectProperty<Ressource> ressource = new SimpleObjectProperty<>();
    private Point coordinate = new Point();

    public Square(int _x, int _y) {
        coordinate.x = _x;
        coordinate.y = _y;
        robotProperty().setValue(null);
        ressource.setValue(null);
    }


    //region Getters and setters
    public Robot getRobot() {
        return robot.get();
    }

    public ObjectProperty<Robot> robotProperty() {
        return robot;
    }

    public void setRobot(Robot robot) {
        this.robot.set(robot);
    }

    public Ressource getRessource() {
        return ressource.get();
    }

    public ObjectProperty<Ressource> ressourceProperty() {
        return ressource;
    }

    public void setRessource(Ressource ressource) {
        this.ressource.set(ressource);
    }

    public Point getCoordinate() {
        return coordinate;
    }

    public void setCoordinate(Point coordinate) {
        this.coordinate = coordinate;
    }
    //endregion


    @Override
    public String toString() {
        StringBuilder res = new StringBuilder();
        res.append("|");
        if (robot != null){
            res.append("■");
        }else {
            res.append(" ");
        }
        if (ressource != null){
            res.append("□");
        }else{
            res.append(" ");
        }
        res.append("|");
        return res.toString();
    }
}
