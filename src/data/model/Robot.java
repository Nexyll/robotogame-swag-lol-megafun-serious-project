package data.model;

import data.command.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Random;
import java.util.stream.Collectors;

public class Robot {
    private Ressource ressource;
    private static final double kPickUp = 2; //0.1
    private static final double kPutDown = 1; //0.3
    private Random rd = new Random();

    public ICommand chooseCommand(PartialEnvironment partialEnvironment)
    {
        ICommand chosenCommand = new NoActionCommand(); // Résultat de la méthode
        Square robotSquare = partialEnvironment.findRobotSquare(this);
        HashMap<TypeRessource, Double> ressourceFrequencyMap = computeRessourceFrequencyMap(partialEnvironment);
        // Si le robot ne transporte rien
        if (ressource == null)
        {
            //Si la case du robot a une ressource
            if (robotSquare.getRessource() != null)
            {
                double pPickUp = kPickUp / Math.pow(kPickUp + ressourceFrequencyMap.get(robotSquare.getRessource().getType()), 2);
                if (rd.nextDouble() <= pPickUp) // si on choisi de prendre la ressource
                {
                    return new PickUpRessourceCommand(this, robotSquare);
                }
                else // On se déplace
                {
                   chosenCommand = makeRandomMove(partialEnvironment, robotSquare);
                }
            }
            else // Sinon on va sur une case de ressource
            {

                ArrayList<Square> possibleMoveSquares = getPossibleMoveSquares(partialEnvironment);
                if (possibleMoveSquares.size() == 0) return new NoActionCommand();


                ArrayList<Square> ressourceSquares = possibleMoveSquares
                        .stream()
                        .filter(x -> x.getRessource() != null)
                        .collect(Collectors.toCollection(ArrayList::new));

                Square targetSquare;

                if (ressourceSquares.isEmpty()) {
                    if (possibleMoveSquares.size() == 0) return new NoActionCommand();
                    if (possibleMoveSquares.size() == 1)
                    {
                        targetSquare = possibleMoveSquares.get(0);
                    }
                    else
                    {
                        targetSquare = possibleMoveSquares.get(rd.nextInt(possibleMoveSquares.size() - 1));
                    }

                    if (targetSquare == robotSquare) chosenCommand = new NoActionCommand();
                    else chosenCommand = new MoveCommand(new Move(robotSquare, targetSquare));
                } else {
                    targetSquare = ressourceSquares.get(0);
                    if (targetSquare != robotSquare)
                        chosenCommand = new MoveCommand(new Move(robotSquare, targetSquare));
                    else chosenCommand = new NoActionCommand(); //TODO replace by TakeCommand

                }
            }
        }
        else // On transporte une ressource
        {
            //double pPutDown = kPutDown / Math.pow(kPutDown + ressourceFrequencyMap.get(ressource.getType()), 2);
            double pPutDown = ressourceFrequencyMap.get(ressource.getType());
            if (pPutDown >= 0.2 && robotSquare.getRessource() == null)
            {
                chosenCommand = new PutDownRessourceCommand(this, robotSquare);

            }
            else
            {
                chosenCommand = makeRandomMove(partialEnvironment, robotSquare);
            }
        }


        return chosenCommand;
    }

    private ICommand makeRandomMove(PartialEnvironment partialEnvironment, Square robotSquare) {
        ArrayList<Square> possibleMoveSquares = getPossibleMoveSquares(partialEnvironment);
        if (possibleMoveSquares.size() == 0) return new NoActionCommand();
        if (possibleMoveSquares.size() == 1) return new MoveCommand(new Move(robotSquare, possibleMoveSquares.get(0)));
        return new MoveCommand(new Move(robotSquare, possibleMoveSquares.get(rd.nextInt(possibleMoveSquares.size()-1))));
    }

    private ArrayList<Square> getPossibleMoveSquares(PartialEnvironment partialEnvironment) {
        ArrayList<Square> possibleMoveSquares = partialEnvironment.getSquares().stream()
                .filter(x -> x.getRobot() == null)
                .collect(Collectors.toCollection(ArrayList::new));
        Collections.shuffle(possibleMoveSquares);
        return possibleMoveSquares;
    }

    private HashMap<TypeRessource,Double> computeRessourceFrequencyMap(PartialEnvironment partialEnvironment) {
        HashMap<TypeRessource, Double> ressourceFrequencyMap = new HashMap<>();
        for (TypeRessource typeRessource : TypeRessource.values()) {
            int ressourceTypeTotalCount = (int) partialEnvironment.getSquares().stream()
                    .filter(x -> x.getRessource() != null)
                    .filter(x -> x.getRessource().getType() == typeRessource)
                    .count();
            ressourceFrequencyMap.put(typeRessource, (ressourceTypeTotalCount / (double)partialEnvironment.getSquares().size()));
        }
        return ressourceFrequencyMap;
    }

    @Override
    public String toString() {
        return "■";
    }

    public Ressource getRessource() {
        return ressource;
    }

    public void setRessource(Ressource ressource) {
        this.ressource = ressource;
    }
}
