package data.model;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

public class Ressource {

    private ObjectProperty<TypeRessource> type = new SimpleObjectProperty<>();

    public Ressource(TypeRessource type)
    {
        this.type.setValue(type);
    }

    //region Getters and setters
    public TypeRessource getType() {
        return type.get();
    }

    public ObjectProperty<TypeRessource> typeProperty() {
        return type;
    }

    public void setType(TypeRessource type) {
        this.type.set(type);
    }
    //endregion
}

