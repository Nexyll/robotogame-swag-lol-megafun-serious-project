package data.model;

public class PartialEnvironment extends Environment {
    public PartialEnvironment(int _x, int _y) {
        super(0, 0);
        sizeX = _x;
        sizeY = _y;
    }

    @Override
    public String toString() {
        return String.valueOf(getSquares().size());
    }
}
