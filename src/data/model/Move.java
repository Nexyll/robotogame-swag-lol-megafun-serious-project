package data.model;

public class Move {
    private Square startSquare;
    private Square targetSquare;

    public Move(Square startSqaure, Square targetSquare) {
        this.startSquare = startSqaure;
        this.targetSquare = targetSquare;
    }


    //region Getters - Setters

    public Square getStartSquare() {
        return startSquare;
    }

    public Square getTargetSquare() {
        return targetSquare;
    }

    //endregion

}
