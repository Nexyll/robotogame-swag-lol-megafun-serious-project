package data.command;

import data.model.Environment;
import data.model.Robot;
import data.model.Square;

public class PutDownRessourceCommand implements ICommand {
    private Robot robot;
    private Square square;

    /**
     *
     * @param robot Robot qui va déposer la ressource
     * @param square Case sur laquelle la ressource va être déposée
     */
    public PutDownRessourceCommand(Robot robot, Square square)
    {
        this.robot = robot;
        this.square = square;
    }

    @Override
    public void execute() throws Exception {
        if (square.getRessource() != null) throw new Exception("La case possède déjà une ressource");
        if (robot.getRessource() == null) throw new Exception("Le robot n'a pas de ressource a déposé");

        square.setRessource(robot.getRessource());
        robot.setRessource(null);

    }

}
