package data.command;

import data.model.Environment;

public interface ICommand {
    /**
     *
     */
    void execute() throws Exception;

}
