package data.command;

import data.model.Environment;
import data.model.Move;
import data.model.Robot;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class MoveCommand implements ICommand {
    private Move move;

    public MoveCommand(Move move){
        this.move = move;
    }

    @Override
    public void execute() throws Exception {
        if (move.getTargetSquare().getRobot() != null) throw new Exception("Un robot est déjà sur la case");
        Robot r = move.getStartSquare().getRobot();
        move.getStartSquare().setRobot(null);
        move.getTargetSquare().setRobot(r);
    }
}
