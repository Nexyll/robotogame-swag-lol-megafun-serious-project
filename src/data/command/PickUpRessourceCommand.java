package data.command;

import data.model.Environment;
import data.model.Robot;
import data.model.Square;

public class PickUpRessourceCommand implements ICommand {
    private Robot robot;
    private Square square;

    /**
     *
     * @param robot Robot qui va prendre la ressource
     * @param square Case sur laquelle se situe la ressource
     */
    public PickUpRessourceCommand(Robot robot, Square square)
    {
        this.robot = robot;
        this.square = square;
    }

    @Override
    public void execute() throws Exception {
        if (square.getRessource() == null) throw new Exception("Plus de ressource dans la case choisie");
        if (robot.getRessource() != null) throw new Exception("Le robot transporte déjà une ressource");

        this.robot.setRessource(this.square.getRessource());
        this.square.setRessource(null);
    }

}
